import { useState } from 'react'
import { Container, Navbar, Nav } from 'react-bootstrap'
import { useSelector } from 'react-redux'

import { NavLink, useNavigate } from 'react-router-dom'
export const NavbarComponent = () => {
  let user_info = JSON.parse(localStorage.getItem("_au")) ?? null
  let navigate = useNavigate()

  let [totalItems, setTotalItems] = useState(0)
  totalItems = useSelector((store) => {
    let cart = store.cart;
    let total = 0
    cart.cart && cart.cart.map((item) => {
      total += Number(item.qty)
    })
    return total
  })
  return (
    <Navbar bg="dark" variant="dark" sticky="top">
      <Container>
        <Navbar.Brand to="#home">HeavenDream</Navbar.Brand>
        <Nav className="me-auto">
          <NavLink className="nav-link" to="/">Home</NavLink>
          {
            !user_info ? <>
              <NavLink className="nav-link" to="/login">Login</NavLink>
              <NavLink className="nav-link" to="/register">Register</NavLink>
            </> : null
          }
        </Nav>
        <Nav>
          <NavLink className={"nav-link"} to={"/cart"}>Cart{totalItems }</NavLink>
          {
            user_info ? <>
              <Nav>
                <NavLink className="nav-link" to={'/' + user_info.role}>{user_info.name}</NavLink>
                <NavLink className="nav-link" to="/login" onClick={(e) => {
                  e.preventDefault()
                  localStorage.clear()
                  navigate('/login')
                }}>Logout</NavLink>
              </Nav>
            </> : null
          }
        </Nav>
      </Container>
    </Navbar>
  )
}