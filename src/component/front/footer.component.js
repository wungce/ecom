import { Container, Navbar, Row, Col, Nav } from 'react-bootstrap';
export const FooterComponent = () => {

    return (
        <>
            <Navbar bg="dark" variant="dark" className="home-footer mt-4">
                <Container>
                    <Row>
                        <Col sm={12} md={4} >
                            <h4>
                                About us
                            </h4>
                            <hr />
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </p>
                        </Col>
                        <Col sm={12} md={4} >
                            <h4>Quick Links</h4>
                            <hr />
                            <Nav className='flex-column'>
                                <Nav.Item>
                                    <Nav.Link>
                                        Home
                                    </Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link>
                                        About us
                                    </Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link>
                                        Privacy Policy
                                    </Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link>
                                        Return Policy
                                    </Nav.Link>
                                </Nav.Item>
                            </Nav>
                        </Col>
                        <Col sm={12} md={4} >
                            <h4>
                                Get Connected
                            </h4>
                            <hr />
                        </Col>
                    </Row>
                </Container>
            </Navbar>
        </> 
    )

}
