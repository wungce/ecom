import React from 'react'
import { Card, Badge, Button } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import imageNotFound from '../../../assets/img/No-image-found.png'
import { NumericFormat } from 'react-number-format';
import '../../../assets/front/css/product.css'
import { useDispatch, useSelector } from 'react-redux';
import { updateCart, updateItemInCart } from '../../../reducers/cart.reducer';
import { toast } from 'react-toastify';


const ProductListCard = ({ item }) => {
    const handleError = (e) => {
        e.target.src = imageNotFound
    }
    const dispatch = useDispatch()

    let cart = useSelector((store) => {
       return store.cart
    })
    const addItemToCart = () => {
        let index = null;

        var all_items = cart.cart;
        all_items && all_items.map((cart_item, ind) => {
            if (item._id === cart_item.product_id) {
             index = ind
            }
        })

        if (index !== null) {
            let total_qty = Number(all_items[index]['qty']) + 1;
            dispatch(updateItemInCart({ index, total_qty, all_items}))
        } else {
            all_items = [
                ...all_items,
                {
                    qty: 1,
                    product_id: item._id
                }
            ];    
            dispatch(updateCart(all_items))
            toast.success("Your car has been updated")
        }
        console.log(all_items)
    }
    return (
        <>
            <Card className='shadow-sm'>
                <NavLink to={"/product/"+item.slug} className="link">
                    <img src={process.env.REACT_APP_IMAGE_URL + "/product/" + item.images[0]} onError={handleError} alt={item.title} width={253} height={190} />
                </NavLink>

                <Card.Title className='text text-center p-2'>
                    <NavLink to={"/product/" + item.slug} className="link">
                        {
                            `${item.title.substring(0, 40)}...`
                        }
                    </NavLink>
                </Card.Title>
                <Card.Text className='h6 p-2'>
                    <p>

                        {
                            item.category && item.category.map((cat, index) => (
                                <NavLink to={`/category/${cat.slug}`} key={index} className="btn-link">
                                    <Badge bg="info" className='mx-2 mt-1'>
                                        {
                                            cat.title
                                        }
                                    </Badge>
                                </NavLink>
                            ))
                        }
                    </p>
                    {
                        item.brand &&
                        <em className='h6'>
                            <NavLink to={`/brand/${item.brand._id}`}>
                                {
                                    item.brand.name
                                }
                            </NavLink>
                        </em>
                    }
                    <p className='py-2'>
                        <NumericFormat value={item.after_discount} displayType={'text'} thousandSeparator={true} prefix={'Npr. '} />
                        {
                            item.price && item.price > 0 &&

                            <del className='text-danger px-1 ml-1'>
                                <NumericFormat value={item.price} displayType={'text'} thousandSeparator={true} prefix={'Npr. '} />
                            </del>
                        }
                    </p>
                    <Button variant='warning' size='sm' onClick={addItemToCart}>
                        Add To Cart
                    </Button>

                </Card.Text>
            </Card>
        </>
    )
}

export default ProductListCard