import { NavbarComponent } from "./nav.component";
import { FooterComponent } from "./footer.component";

const FrontComponent = {
    NavbarComponent,
    FooterComponent
}

export default FrontComponent