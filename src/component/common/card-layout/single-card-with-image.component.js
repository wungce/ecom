import React from 'react'
import { Card } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import imageNotFound from '../../../assets/img/No-image-found.png'


const CardWithImage = ({item, type}) => {
    const handleError = (e) => {
        e.target.src = imageNotFound
    }
    return (
        <>
            <Card className='shadow-sm'>
                <NavLink to={"/"+type+"/"+item.slug} className="link">
                    <img  src={process.env.REACT_APP_IMAGE_URL+"/"+type+"/"+item.image} onError={handleError} alt={item.title} width={150} height={120} />
                    <p className="text-center pt-2">{item.title}</p>
                </NavLink>
            </Card>
        </>
    )
}

export default CardWithImage