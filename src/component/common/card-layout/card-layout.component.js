import React from 'react'
import { Container, Row, Col} from 'react-bootstrap'
import CardWithImage from './single-card-with-image.component'


const CardLayout = ({ data, type }) => {
    return (
        <>
            <Container>
                <Row>
                    {
                        data && data.map((item, i) => (
                            <Col sm={12} md={2} key={i}>
                                <CardWithImage item={item} type={type}/>
                            </Col>
                        ))
                    }
                </Row>
            </Container>
        </>
    )
}

export default CardLayout