import { Carousel } from 'react-bootstrap'
import ImageNotFound from '../../../assets/img/No-image-found.png'

const SliderComponent = ({ data, type }) => {
    const handleError = (e) => {
        e.target.src = ImageNotFound
    }
    return (
        <>
            <Carousel fade>
               {
                data && data.map((item, index) =>(
                    <Carousel.Item key={index}>
                    <img
                        className="d-block w-100"
                        src={process.env.REACT_APP_IMAGE_URL+"/"+type+"/"+item.image}
                        alt="Second slide"
                        height={280}
                        onError={handleError}
                    />
                </Carousel.Item>
                ))
               }
      
            </Carousel>

        </>
    )
}

export default SliderComponent