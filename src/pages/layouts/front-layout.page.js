import { useDispatch } from "react-redux"
import { Outlet } from "react-router-dom"
import FrontComponent from "../../component/front"
import { setCart } from "../../reducers/cart.reducer"

const FrontLayout =  () => {
    let dispatch = useDispatch()
    let cart = JSON.parse(localStorage.getItem('cart_8')) || []
    dispatch(setCart(cart))
return (
    <>
    <FrontComponent.NavbarComponent/>
    <Outlet />
    <FrontComponent.FooterComponent/>
    
    </>

)
}
export default FrontLayout