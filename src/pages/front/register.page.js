import { useState, useEffect } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { NavLink, useNavigate } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

import { httpPostRequest } from "../../services/axios.service";

const RegisterPage = () => {
  let default_data = {
    name: '',
    email: '',
    password: '',
    role:'',
    image: null
  };
  let [data, setData] = useState(default_data);
  let [err, setErr] = useState({
    name : '',
    email: '',
    password: '',
    role : '',
  })
  
  let navigate = useNavigate()
  const passEvent = (e) => {
    let { name, value } = e.target;
    setData({
      ...data,
      [name]: value,
    });
    validation(name, value)
  };

  const validation = (field, value) => {
    let msg = ''
    switch (field) {
      case "name":
        msg = !value ? "Name is required" : null
        break;
      case "email":
        msg = !value ? "Email is required" : (/^[^\s@]+@[^\s@]+\.[^\s@]+$/).test(value) ? "" : "Invalid email format."
        break;

      case "password":
        msg = !value ? "Password is required" : (value.length > 2) ? "" : "Password must be 8 charactors"
        break;
      case "role":
        msg = !value ? "Role is required" : (value !== 'customer' || value !== 'seller' ? "Undefined roles" : ' ')
        break;
      default:
        break;
    }
    setErr({
      ...err,
      [field]: msg
    })

  }
  const handleSubmit = async (e) => {
    e.preventDefault()
    try {
      console.log(data)
      let form_data = new FormData()

      if (data.image) {
        form_data.append('image', data.image, data.image.name);
        delete data.image
      }
      Object.keys(data).map((key) => {
        form_data.append(key, data[key]);
        return null;
      })
      let response = await httpPostRequest('/register', form_data, false, true)
      console.log(response)
      if (response.status) {
        toast.success(response.msg)
        navigate('/login')
      }
    } catch (err) {
      toast.error(err.response.data.msg)
      console.error(err.response.data.msg)
    }

  }
  useEffect(() => {
    let token = localStorage.getItem('access_token');
    if (token) {
      let user_info = JSON.parse(localStorage.getItem('_au'))
      navigate('/'+user_info.role)
    }
  }, [navigate])

  return (
    <>
      <Container>
        <ToastContainer />
        <Row className="mt-3">
          <Col sm={{ offset: 3, span: 7 }}>
            <h1>Register form</h1>
            <hr />
            <Form onSubmit={handleSubmit}>
              <Form.Group className="mb-3" controlId="name">
                <Form.Label>Name:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter your name"
                  name="name"
                  size="sm"
                  required
                  onChange={(e) => {
                    let value = e.target.value;
                    setData({
                      ...data,
                      name: value,
                    });
                    validation("name", value)
                  }}
                />
                <em className="text-danger">{err ? err.name : ""}</em>
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  name="email"
                  size="sm"
                  required
                  onChange={(e) => {
                    let value = e.target.value;
                    setData({
                      ...data,
                      email: value,
                    });
                    validation("email", value)
                  }}
                />
                <em className="text-danger">{err ? err.email : ""}</em>
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  name="password"
                  size="sm"
                  required
                  onChange={passEvent}
                />
                <em className="text-danger">{err ? err.password : ""}</em>

              </Form.Group>
              <Form.Group className="mb-3" controlId="role">
                <Form.Label>Role</Form.Label>
                <Form.Select name='role' required onChange={(e) => {
                  let value = e.target.value;
                  setData({
                    ...data,
                    role: value,
                  });
                  validation("role", value)
                }}>
                  <option value="seller">Seller</option>
                  <option value="customer">Customer</option>
                </Form.Select>

              </Form.Group>
              <Form.Group className="mb-3" controlId="image">
                <Form.Label>Image:</Form.Label>
                <Form.Control
                  type="file"
                  name="image"
                  size="sm"
                  onChange={(e) => {
                    let file = e.target.files[0]
                    setData({
                      ...data,
                      image: file
                    })
                  }}
                />

                <em className="text-danger">{err ? err.image : ""}</em>
                <Col sm={3}>
                  <img src={data.image ? URL.createObjectURL(data.image) : ""} className="img img-fluid" alt="" />

                </Col>
              </Form.Group>

              <div className="d-grid gap-2">
                <Button variant="btn btn-outline-success" type="submit">
                  Submit
                </Button>OR <NavLink to="/login">Login</NavLink>
              </div>
            </Form>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default RegisterPage;
