// totally import css of bootstrap import
import "bootstrap/dist/css/bootstrap.min.css"
// completely venila javascript import of css
// import "bootstrap"
import React from "react";

class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        name : this.props.name,
        role : "Admin",
        address : "Satungal"
    }
    console.log("I'm a first call.")
  }
  updateFunction = () => {
    this.setState({
        ...this.state,
        name : "Gopal"
    })

  }
  componentDidMount = () => {
    console.log("First call function.")
  }
  componentDidUpdate = () => {
    console.log("Third call function.")
  }
  componentWillUnmount = () => {
    console.log("Fourth call function.")
  }

  render = () => {
    console.log("I'm a render function.")
    return (
        <>
        <p>
            name : {this.state.name},
            role : {this.state.role},
            address : {this.state.address}
            props from tag : {this.props.name}
        <br />
        {/* <span onClick={this.updateFunction}>Click me</span> */}
        <button onClick={this.updateFunction}>Click me</button>
        </p>
        </>
    )
  };
}

export default Home;
