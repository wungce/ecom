// import HomePage from "./home.page1"
import HomePage from "./home.page"
import LoginPage from "./login.page"
import RegisterPage from "./register.page"
import CategoryPage from "./category-detail.page"
import ErrorPage from "./error.page"
import ProductPage from './product-detail.page'
import CartPage from "./cart.page"

const FrontPage = {
    HomePage,
    LoginPage,
    RegisterPage,
    CategoryPage,
    ErrorPage,
    ProductPage,
    CartPage

    
}

export default FrontPage