import React, { useCallback, useEffect, useState } from 'react'
import { Container, Row, Col, Badge, Form, Button } from 'react-bootstrap'
import SliderComponent from '../../component/common/slider/slider.component'
import { NavLink, useParams } from 'react-router-dom'
import { NumericFormat } from 'react-number-format';
import ProductListCard from '../../component/front/product-list/product-list.component';
import { httpGetRequest } from '../../services/axios.service';
import { useDispatch } from 'react-redux';
import { addToCart } from '../../reducers/cart.reducer';
const ProductDetailPage = () => {
    const [data, setData] = useState(null)
    const [sliderData, setSliderDate] = useState()
    const [relatedProducts, setRelatedProducts] = useState()
    let params = useParams()
    let [qty, setQty] = useState(0)
    const dispatch = useDispatch()

    const getProductDetail = useCallback(async () => {
        let slug = params.slug;
        let response = await httpGetRequest('/product/detail/' + slug)
        console.log(response)
        if (response.status) {
            setData(response.result.products)
            setRelatedProducts(response.result.related_product)
            let slider = []
            response.result.products.images.map((item) => {
                slider.push({
                    image: item
                })
            })

            setSliderDate(slider)
        }
    }, [])

    const addQty = (e) => {
         setQty(e.target.value)
    }

    const handleCartAdd = (e) => {
        e.preventDefault()
        dispatch(addToCart({qty:qty, product_id: data._id}))


    }
    useEffect(() => {
        getProductDetail()
    }, [params])
    return (
        <>
            {
                data && data !== null ? <>  <Container className='mt-5 '>
                    <Row>
                        <Col sm={12} md={6}>
                            {
                                data && data.images &&
                                <SliderComponent
                                    type="product"
                                    data={sliderData}
                                />
                            }
                        </Col>
                        <Col sm={12} md={6}>
                            <h4>{data.title}</h4>
                            <hr />
                            <Row>
                                <Col>
                                    {
                                        data.category && data.category.map((cat, index) => (
                                            <NavLink to={`/category/${cat.slug}`} key={index} className="btn-link">
                                                <Badge bg="info" className='mx-2 mt-1'>
                                                    {
                                                        cat.title
                                                    }
                                                </Badge>
                                            </NavLink>
                                        ))
                                    }
                                </Col>
                                <Col>
                                    <Badge bg="warning"> {data.brand.name}</Badge>
                                </Col>
                                <Row className='mt-5'>
                                    <Col sm={3} className="h5">Price: </Col>
                                    <Col sm={9}>
                                        <NumericFormat value={data.after_discount} displayType={'text'} thousandSeparator={true} prefix={'Npr. '} />

                                        {
                                            // data.discount.discount_value && data.discount.discount_value > 0 &&

                                            <del> <NumericFormat className='text-danger mx-1' value={data.price} displayType={'text'} thousandSeparator={true} prefix={'Npr. '} /> </del>
                                        }
                                    </Col>
                                </Row>
                                <Row className='mt-5'>
                                   <Form onSubmit={handleCartAdd}>
                                   <Col sm={12} md={6}>
                                        <Form.Control name="qty" min="1" type="number" required placeholder="Enter Product Quantity"
                                        onChange={addQty}
                                        />
                                    </Col>
                                    <Col sm={12} md={6} className="mt-3">

                                        <Button type="submit" variant="warning">
                                            Add To Cart
                                        </Button>
                                    </Col>
                                   </Form>
                                </Row>
                            </Row>
                        </Col>
                    </Row>
                    <Row className='mt-5'>
                        <Col>
                            <h4 className='text-center'>
                                description
                            </h4>
                            <hr />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12} dangerouslySetInnerHTML={{ __html: data.description }}></Col>
                    </Row>
                    <Row className='mt-5'>
                        <Col>
                            <h4>
                                Related Products
                            </h4>
                            <hr />
                        </Col>
                    </Row>

                    <Row className='mt-5'>
                        {
                            relatedProducts && relatedProducts.map((item, index) => (
                                <Col sm={6} md={3} className='mt-2' key={index}>
                                    <ProductListCard
                                        item={item} />
                                </Col>
                            ))
                        }
                    </Row>
                </Container> </> 
                :
                    <>
                        <Row>
                            <Col>
                                <h1>....</h1>
                            </Col>
                        </Row>

                    </>

            }
        </>
    )
}

export default ProductDetailPage  