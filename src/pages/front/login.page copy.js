import { useEffect, useState } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { NavLink, useNavigate } from 'react-router-dom'
// import { useFormik } from "formik";
// import * as Yup from "yup"
import { httpPostRequest } from "../../services/axios.service";
import { toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
// css of toast
const LoginPage = (props) => {
  let navigate = useNavigate()
  let default_data = {
    email: null,
    password: null,
    remember_me: false,
  };
  let [data, setData] = useState(default_data);
  let [err, setErr] = useState({
    email: '',
    password: ''
  })
  // let [loading, setLoading] = useState(false)

  const passEvent = (e) => {
    let { name, value } = e.target;
    setData({
      ...data,
      [name]: value,
    });
    validation(name, value)
  };

  const validation = (field, value) => {
    let msg = ''
    switch (field) {
      case "email":
        msg = !value ? "Email is required" : (/^[^\s@]+@[^\s@]+\.[^\s@]+$/).test(value) ? "" : "Invalid email format."
        break;

      case "password":
        msg = !value ? "Password is required" : ""
        // msg = !value ? "Password is required" :(value.length > 8)? "" : "Password must be 8 charactors"
        break;
      default:
    }
    setErr({
      ...err,
      [field]: msg
    })

  }
  const handleSubmit = async (e) => {
    try {
      e.preventDefault()
      const response = await httpPostRequest('/login', data)
      if (response.status) {
        let user_info = {
          id: response.result.user._id,
          name: response.result.user.name,
          image: response.result.user.image,
          role: response.result.user.role[0]
        }
        localStorage.setItem('access_token', response.result.access_token)
        localStorage.setItem('_au', JSON.stringify(user_info))
        toast.success(response.msg)
        navigate('/' + user_info.role)
      }

    } catch (error) {
      // console.log("err:", error)
      toast.error(error.response.data.msg)
      // console.error(error.response.data.msg)
    }
  }
  useEffect(() => {
    let token = localStorage.getItem('access_token');
    if (token) {
      let user_info = JSON.parse(localStorage.getItem('_au'))
      navigate('/' + user_info.role)
    }
  }, [navigate])
  // console.log(data);
  return (
    <>
      {data.loading ? (
        "Loding..."
      ) : (
        <Container >
          <Row className="mt-5">
            <Col sm={{ offset: 3, span: 7 }}>
              <h1>Login form</h1>
              <hr />
              <Form onSubmit={handleSubmit}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    name="email"
                    size="sm"
                    required
                    onChange={(e) => {
                      let value = e.target.value;
                      setData({
                        ...data,
                        email: value,
                      });
                      validation("email", value)
                    }}
                  />
                  <em className="text-danger">{err ? err.email : ""}</em>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    name="password"
                    size="sm"
                    required
                    onChange={passEvent}
                  />
                  <em className="text-danger">{err ? err.password : ""}</em>

                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                  <Form.Check
                    type="checkbox"
                    label="Remember me"
                    name="remember_me"
                    onChange={(e) => {
                      if (e.target.checked) {
                        setData({
                          ...data,
                          remember_me: true,
                        });
                      } else {
                        setData({
                          ...data,
                          remember_me: false,
                        });
                      }
                    }}
                  />
                </Form.Group>
                <div className="d-grid gap-2">
                  <Button variant="btn btn-outline-success" type="submit">
                    Submit
                  </Button>OR <NavLink to="/register">Create A new Account</NavLink>
                </div>
              </Form>
            </Col>
          </Row>
        </Container>
      )}
    </>
  );
};

export default LoginPage;
