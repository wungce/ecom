import React, { useState } from 'react'
import { useEffect } from 'react';
import { useCallback } from 'react';
import {httpPostRequest} from '../../../src/services/axios.service'
import { Container, Row, Col } from 'react-bootstrap'
import DataTable from 'react-data-table-component';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
const CartPage = () => {
    const columns = [
        {
            name: 'Name',
            selector: row => row.title,
            sortable: true
        },
        {
            name : "Price",
            selector : row => "NPR. "+ row.price
        },
        {
            name : "Qty",
            selector : row =>  row.qty
        },
        {
            name : "Total",
            selector : row => "NPR. "+ row.total
        },
       
    ];
    let [detail, setDetail] = useState([])
     let cartItems = useSelector((store) => {
        return store.cart.cart
     })
    const getCartDetail = useCallback(async () => {
        try{
            let cartDetail = await  httpPostRequest('/cart/detail', cartItems)
            if(cartDetail){
                setDetail(cartDetail.result)
            }
        }catch(error){
            console.log("Error", error)
        } 
   }, [cartItems])
   useEffect(() => {
    getCartDetail()
   }, [getCartDetail])
    return (
        <>
            <Container>
                <Row>
                    <Col>
                        <h1>
                            Order list
                        </h1>
                    </Col>
                </Row>
                <hr />
                <Row>
                    <Col>
                        <DataTable columns={columns} 
                        data={detail} />
                    </Col>
                </Row>
                <hr/>
                <Row>
                    <Col sm={12} md={{offset:10, span:2}}> 
                    <NavLink to="/checkout" className="btn btn-success">
                            checkout
                    </NavLink>
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default CartPage