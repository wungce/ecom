import React, { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import SliderComponent from '../../component/common/slider/slider.component'

import { httpGetRequest } from '../../services/axios.service'
import CardLayout from '../../component/common/card-layout/card-layout.component'
import { Container, Row, Col } from 'react-bootstrap'
import '../../assets/front/css/home.css'
import ProductListCard from '../../component/front/product-list/product-list.component'




const Home = () => {

  const [banner, setBanner] = useState()
  const [cats, setCats] = useState()
  const [featuredProduct, setFeaturedProduct] = useState()

  const getAllBanners = async () => {
    try {
      let all_banners = await httpGetRequest('/banner')
      if (all_banners) {
        let active_banner = all_banners.result.filter((item) => item.status === 'active')

        setBanner(active_banner)
      }
    } catch (error) {
      console.log("Banner fetch error:", error)
    }
  }

  const getAllCategories = async () => {
    try {
      let all_cats = await httpGetRequest('/category')

      if (all_cats) {
        let home_display_cats = all_cats.result.filter((item) => item.status === 'active' && item.show_in_home === true);
        setCats(home_display_cats)
      }
    } catch (error) {
      console.log("Category fetch error:", error)
    }

  }
  const getAllProducts = async () => {
    try {
      let all_products = await httpGetRequest('/product')
      if (all_products.result) {
        let active_featured_product = all_products.result.filter((item) => (item.status === "active" && item.is_featured === true))
        setFeaturedProduct(active_featured_product)
      }

    } catch (error) {
      console.log("Product fetch error:", error)
    }
  }

  useEffect(() => {
    getAllBanners()
    getAllCategories()
    getAllProducts()

  }, [])

  return (
    <>

      <SliderComponent data={banner} type="banner"/>

      <Container className='mt-5'>
        <Row>
          <Col sm={12}>
            <h4 className='text-center'>
              Categories
              <hr />
            </h4>
          </Col>
        </Row>
        <Row>
          <Col sm={12}>
            <CardLayout data={cats} type="category" />

          </Col>
        </Row>
      </Container>

      <Container className='mt-5'>
        <Row>
          <Col sm={12}>
            <h4 className='text-center'>
              Featured Products
              <hr />
            </h4>
          </Col>
        </Row>
        <Row>
          {
            featuredProduct && featuredProduct.map((item, index) => (
              <Col sm={6} md={3} className='mt-2' key={index}>
                <ProductListCard 
                item={item} />
              </Col>
            ))
          }
        </Row>
      </Container>

    </>
  )
}

export default Home