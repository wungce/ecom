import { useEffect, useState } from "react"
import { useParams, useSearchParams } from "react-router-dom"
import { Container, Row, Col } from "react-bootstrap"
import {httpGetRequest} from "../../services/axios.service"

import ProductListCard from "../../component/front/product-list/product-list.component"
const CategoryPage = () => {
    let params = useParams()

    let [productLists, setProductLists] = useState()
    let getAllProductsByCat = async () => {
      try{
         let result = await httpGetRequest('/product/cat/'+params.slug)
         if(result){
          setProductLists(result.result)
         }

      } catch(error){
        console.log("Product Fetch Error:", error)
      }
    }

    useEffect(() => {
      getAllProductsByCat()
    }, [])
    return (
        <>
      <Container className='mt-5'>
        <Row>
          <Col sm={12}>
            <h4 className='text-center'>
              Product s of <em>{params.slug}</em> Category
            </h4>
              <hr />
          </Col>
        </Row>
        <Row>
          {
            productLists && productLists.map((item, index) => (
              <Col sm={6} md={3} className='mt-2' key={index}>
                <ProductListCard 
                item={item} />
              </Col>
            ))
          }
        </Row>
      </Container>
        </>
    )
}

export default CategoryPage