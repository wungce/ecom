import { useFormik } from "formik"
import { Button, Col, Form } from "react-bootstrap"
import Breadcrumb from "../partials/breadcrumb.partials"
import * as Yup from 'yup'
import { httpGetRequest, httpPutRequest } from '../../../services/axios.service'
import { toast } from "react-toastify"
import { useNavigate, useParams } from "react-router-dom"
import React, { useCallback, useEffect, useState } from "react"

import Select from 'react-select'

import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
let default_value = {
    title: '',
    status: '',
    image: '',
    brand: '',
    category: '',
    is_featured: false,
    price: '',
    description: '',
    discount_type: 'percent',
    discount_value: '',
    stock: 0,
    seller: ''

}
const ProductEditComponent = () => {
    let param = useParams()

    let validationSchema = Yup.object().shape({
        title: Yup.string().required('Title is required.'),
        status: Yup.string().required("Status is required"),
        image: Yup.object().nullable(),
        brand: Yup.object().nullable(),
        category: Yup.array().nullable(),
        is_featured: Yup.boolean(),
        price: Yup.number().required('Price is required'),
        description: Yup.string().required("Description is required")
    })

    let [allCats, setAllCats] = useState()
    let [allBrands, setAllBrands] = useState();
    let [allSellers, setAllSellers] = useState()
    let [loading, setLoading] = useState(true)

    let getAllCategories = useCallback(
        async () => {
            try {
                let response = await httpGetRequest('/category', true)
                if (response.status) {
                    let cats = response.result.map((item) => {
                        return {
                            label: item.title,
                            value: item._id
                        }
                    })
                    setAllCats(cats)
                }
            } catch (err) {
                console.error("Error :", err)
            }finally{
                setLoading(!loading)
            }
        }
        , [])

    let getAllBrands = useCallback(async () => {
        try {
            let response = await httpGetRequest('/brand', true)
            if (response) {
                let brands = response.result.map((item) => {
                    return {
                        label: item.name,
                        value: item._id
                    }
                })
                setAllBrands(brands)
            }
        } catch (error) {
            console.error(error)
        }finally{
            setLoading(!loading)
        }
    }, [])

    let sellerUsers = useCallback(async () => {
        try {
            let response = await httpGetRequest('/user?role=seller', true)
            if (response.result) {
                let sellers = response.result
                // let sellers = response.result.filter((item) => item.role.include('seller'))
                let users = sellers.map((item) => {
                    return {
                        label: item.name,
                        value: item._id
                    }
                })
                setAllSellers(users)
            }

        } catch (error) {
            console.log("User fetch error:", error)
        }finally{
            setLoading(!loading)
        }
    }, [])

    let navigate = useNavigate()

    const formik = useFormik({
        initialValues: default_value,
        validationSchema: validationSchema,
        onSubmit: async (values) => {
            try {
                const form_data = new FormData()

                if (values.image) {

                    // multiple image append in form data
                    values.image.map((item) => {
                        form_data.append('image', item, item.name)
                        return null
                    })
                    // single image append if form this method
                    // form_data.append('image', values.image, values.image.name)
                    delete values.image
                }else{
                    delete values.images
                }

                if (values.brand) {
                    values.brand = values.brand.value;
                }

                if (values.category) {
                    values.category = values.category.map((item) => item.value)
                }

                if (values.seller) {
                    values.seller = values.seller.value
                }
                if(values.discount){
                    delete values.discount
                }

                Object.keys(values).map((key) => {
                    form_data.append(key, values[key])
                    return null;
                })
                let response = await httpPutRequest('/product/'+param.id, form_data, true, true)
                if (response.status) {
                    toast.success(response.msg)
                    navigate('/admin/product')
                } else {
                    toast.error(response.msg)
                }    
            } catch (err) {
                console.error("Error: ", err)
            }
        }
    })

    let getProductDetail = useCallback(
        async () => {
            let id = param.id
            try{
                let response = await httpGetRequest('/product/'+id)
                if(response.status){
                    let sel_cat = response.result.category.map((item) => {
                        return {
                            label : item.title,
                            value : item._id
                        }
                    })

                    let sel_brands = {
                        label : response.result.brand.name,
                        value : response.result.brand._id
                    } 
                    formik.setValues({
                        ...formik.values,
                        ...response.result,
                        category: sel_cat,
                        brand : sel_brands,
                        discount_type : response.result.discount.discount_type ?? "percent",
                        discount_value : response.result.discount.discount_value ?? 0
                    })
                }
            } catch(error){
                console.log(error)  
            }
        },
   [param.id, formik] )

    useEffect(() => {    
        getAllCategories()
        getAllBrands()
        sellerUsers()
    }, [getAllCategories, getAllBrands, sellerUsers])
    
    useEffect(() => {
        getProductDetail()
    }, [loading])

    return (
        <div className="container-fluid px-4">
            <Breadcrumb
                context="Product"
                createUrl=""
                type="update"
            />
            <div className="card mb-4">
                <div className="card-body">
                    <form onSubmit={formik.handleSubmit}>
                        <Form.Group className="row mb-3" controlId="title">
                            <Form.Label className="col-sm-3">Title:</Form.Label>
                            <Col sm={9}>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter Product Title"
                                    name="title"
                                    size="sm"
                                    required
                                    value={formik.values.title}
                                    onChange={formik.handleChange}
                                />
                                {
                                    formik.errors.title && <em className="text-danger">{formik.errors.title}</em>
                                }
                            </Col>
                        </Form.Group>

                        {/* Description */}
                        <Form.Group className="row mb-3" controlId="description">
                            <Form.Label className="col-sm-3">Description:</Form.Label>
                            <Col sm={9}>
                                <CKEditor
                                    editor={ClassicEditor}
                                    data={formik.values.description}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        formik.setValues({
                                            ...formik.values,
                                            description: data
                                        })
                                    }}
                                    name="description"
                                />
                                {
                                    formik.errors.description && <em className="text-danger">{formik.errors.description}</em>
                                }
                            </Col>
                        </Form.Group>

                        {/* Category */}
                        <Form.Group className="row mb-3" controlId="category">
                            <Form.Label  className="col-sm-3">Category:</Form.Label>
                            <Col sm={9}>
                                <Select options={allCats}
                                    isMulti
                                    onChange={(e) => {
                                        formik.setValues({
                                            ...formik.values,
                                            category: e
                                        })
                                    }}
                                    value={formik.values.category}
                                    name="category"
                                    required
                                />


                                {
                                    formik.errors.category && <em className="text-danger">{formik.errors.category}</em>
                                }
                            </Col>
                        </Form.Group>

                        {/* price */}
                        <Form.Group className="row mb-3" controlId="price">
                            <Form.Label className="col-sm-3">Price(NPR.):</Form.Label>
                            <Col sm={9}>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Product Price"
                                    name="price"
                                    size="sm"
                                    min="1"
                                    required
                                    value={formik.values.price}
                                    onChange={formik.handleChange}
                                />
                                {
                                    formik.errors.price && <em className="text-danger">{formik.errors.price}</em>
                                }
                            </Col>
                        </Form.Group>

                        {/* Discount */}
                        <Form.Group className="row mb-3" controlId="discount_type">
                            <Form.Label className="col-sm-3">Discount:</Form.Label>
                            <Col sm={2}>
                                <Form.Select
                                    name="discount_type"
                                    size='sm'
                                    value={formik.values.discount_type}
                                    onChange={formik.handleChange}
                                >
                                    <option>--select any--</option>
                                    <option value="percent">Percent(%)</option>
                                    <option value="flat">in sum</option>
                                </Form.Select>
                            </Col>

                            <Col sm={7}>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Product Discount"
                                    name="discount_value"
                                    size="sm"
                                    min="0"
                                    value={formik.values.discount_value}
                                    onChange={formik.handleChange}
                                />
                                {
                                    formik.errors.discount_value && <em className="text-danger">{formik.errors.discount_value}</em>
                                }
                            </Col>
                        </Form.Group>

                        <Form.Group className="row mb-3" controlId="status">
                            <Form.Label  className="col-sm-3">Status :</Form.Label>
                            <Col sm={9}>
                                <Form.Select size="sm" value={formik.values.status} onChange={formik.handleChange}>
                                    <option value="">--select Any One</option>
                                    <option value="active">Active</option>
                                    <option value="inactive">In-Active</option>
                                </Form.Select>
                                {
                                    formik.errors.status && <em className="text-danger">{formik.errors.status}</em>
                                }
                            </Col>
                        </Form.Group>

                        {/* brand */}
                        <Form.Group className="row mb-3" controlId="brand">
                            <Form.Label  className="col-sm-3">Brand :</Form.Label>
                            <Col sm={9}>
                                <Select options={allBrands}
                                    onChange={(e) => {
                                        formik.setValues({
                                            ...formik.values,
                                            brand: e
                                        })
                                    }}
                                    value={formik.values.brand}
                                    name="brand"
                                    required
                                />
                                {
                                    formik.errors.brand && <em className="text-danger">{formik.errors.brand}</em>
                                }
                            </Col>
                        </Form.Group>

                        {/* stock */}
                        <Form.Group className="row mb-3" controlId="stock">
                            <Form.Label className="col-sm-3">Stock:</Form.Label>
                            <Col sm={9}>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Product Stock"
                                    name="stock"
                                    size="sm"
                                    min="0"
                                    value={formik.values.stock}
                                    onChange={formik.handleChange}
                                />
                                {
                                    formik.errors.stock && <em className="text-danger">{formik.errors.stock}</em>
                                }
                            </Col>
                        </Form.Group>

                        <Form.Group className="row mb-3" controlId="is_featured">
                            <Form.Label className="col-sm-3">Is Featured :</Form.Label>
                            <Col sm={9}>
                                <Form.Check
                                    type="checkbox"
                                    label="yes"
                                    name="is_featured"
                                    checked={formik.values.is_featured}
                                    onChange={formik.handleChange}
                                />


                                {
                                    formik.errors.is_featured && <em className="text-danger">{formik.errors.is_featured}</em>
                                }
                            </Col>
                        </Form.Group>

                        {/* seller */}
                        <Form.Group className="row mb-3" controlId="seller">
                            <Form.Label className="col-sm-3">Seller :</Form.Label>
                            <Col sm={9}>
                                <Select options={allSellers}
                                    onChange={(e) => {
                                        formik.setValues({
                                            ...formik.values,
                                            seller: e
                                        })
                                    }}
                                    value={formik.values.seller}
                                    name="seller"
                                    required
                                />
                                {
                                    formik.errors.seller && <em className="text-danger">{formik.errors.seller}</em>
                                }
                            </Col>
                        </Form.Group>

                        <Form.Group className="row mb-3" controlId="image">
                            <Form.Label className="col-sm-3">Image:</Form.Label>
                            <Col sm={3}>
                                <Form.Control
                                    type="file"
                                    name="image"
                                    size="sm"
                                    multiple
                                    onChange={(e) => {
                                        let images = Object.values(e.target.files)
                                        formik.setValues({
                                            ...formik.values,
                                            image: images
                                        })
                                    }}
                                />
                                {
                                    formik.errors.image && <em className="text-danger">{formik.errors.image}</em>
                                }
                            </Col>
                            <Col sm={3}>
                                {
                                    //    it is  single image thumbnil 
                                    //  Product dose not include image
                                    // <img className="img img-fluid" src={formik.values.image && URL.createObjectURL(formik.values.image)} alt="" />
                                }
                            </Col>
                        </Form.Group>

                        <Form.Group className="row mb-3" controlId="submit">
                            <Col sm={{ offset: 3, span: 9 }}>
                                <Button variant="danger" size="sm" type="reset" className="me-2">
                                    <i className="fa fa-trash "></i>Rest
                                </Button>
                                <Button variant="success" size="sm" type="submit">
                                    <i className="fa fa-paper-plane "></i>Submit
                                </Button>
                            </Col>
                        </Form.Group>


                    </form>
                </div>
            </div>
        </div>
    )
}

export default ProductEditComponent