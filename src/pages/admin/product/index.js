import ProductListComponent from "./product-list.page";
import ProductLayout from "./product-layout.page";
import ProductCreateComponent from "./product-create.page";
import ProductEditComponent from "./product-edit.component";

const Product = {
    ProductListComponent,
    ProductLayout,
    ProductCreateComponent,
    ProductEditComponent
    
}
export default Product