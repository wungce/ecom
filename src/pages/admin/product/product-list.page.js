import Breadcrumb from "../partials/breadcrumb.partials"
import DataTable from 'react-data-table-component';
import { useState, useEffect } from "react";
import { httpDeleteRequest, httpGetRequest } from "../../../services/axios.service";
// import { Imageview } from "../../../component/common/image-view/image-view.component";
import { ActionButtons } from "../../../component/common/action-btns/action-buttons.component";
import { toast } from "react-toastify";

const ProductListComponent = () => {
    const deleteProduct = async (id) => {
        try{
            let response = await httpDeleteRequest('/product/'+id, true)
            console.log("response",response)
            if(response.status){
                toast.success(response.msg)
                getAllProducts()
            } else{
                toast.error(response.msg)
            }
        } catch(err){
            console.error("DeleteErr:", err)
        }
    }
    const columns = [
        {
            name: 'Name',
            selector: row => row.title,
            sortable: true
        },
        {
            name: 'Category',
            selector: row => row.category ? row.category.map((item) => item.title).join(', ') : "-"
        },
        {
            name : "Price",
            selector : row => "NPR. "+ row.after_discount.toFixed(2)
        },
        {
            name : "Featured",
            selector : row => row.is_featured ? "Yes" : "No"
        },
        {
            name : "Brand",
            selector : row => row.brand ? row.brand.name : "-"
        },
         {
            name: 'Status',
            selector: row => row.status,
            sortable: true
        },
        {
            name: 'Action',
            selector: row => <ActionButtons id={row._id} deleteAction={deleteProduct} path="/admin/product/" />,
        }
    ];

    const getAllProducts = async () => {
        try{
            let response = await httpGetRequest('/product')
            if(response.status ){
                setData(response.result)
            }
        } catch(err){
            console.log("Exception: ", err)
        }
    }

    const [data, setData] = useState()
    useEffect(() => {
        getAllProducts()
    }, [])
       
    return (
        <>
            <div className="container-fluid px-4">
                <Breadcrumb
                    context="Product"
                    createUrl="/admin/product/create"
                    type="list"
                />
                <div className="card mb-4">
                    <div className="card-body">
                        <DataTable
                            columns={columns}
                            data={data}
                            pagination
                        />
                    </div>
                </div>
            </div>
        </>
    )
}

export default ProductListComponent