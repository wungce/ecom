import Banner from "./banner";
import Brand from "./brand";
import Category from "./category";
import Product from "./product";
import Order from './order'
import User from './users'

import DashboardPage from "./dashboard.page";
const AdminPages = {
    Banner,
    DashboardPage,
    Brand,
    Category,
    Product,
    Order,
    User
}

export default AdminPages