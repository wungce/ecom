import Breadcrumb from "../partials/breadcrumb.partials"
import DataTable from 'react-data-table-component';
import { useState, useEffect } from "react";
import { httpDeleteRequest, httpGetRequest } from "../../../services/axios.service";
import { Imageview } from "../../../component/common/image-view/image-view.component";
import { ActionButtons } from "../../../component/common/action-btns/action-buttons.component";
import { toast } from "react-toastify";

const CategoryListComponent = () => {
    const deleteCategory = async (id) => {
        try{
            let response = await httpDeleteRequest('/category/'+id, true)
            if(response.status){
                toast.success(response.msg)
                getAllCategories()
            } else{
                toast.error(response.msg)
            }
        } catch(err){
            console.error("DeleteErr:", err)
        }
    }
    const columns = [
        {
            name: 'Name',
            selector: row => row.title,
            sortable: true
        },
        {
            name: 'Parent',
            selector: row => row.child_of ? row.child_of.title : "-"
        },
        {
            name : "Brand",
            selector : row => row.brands ? row.brands.map((item) => item.name).join(",") : "-"
        },
        {
            name: 'Image',
            selector: row => <Imageview path={row.image} dir="category"/>,
        },
        {
            name: 'Status',
            selector: row => row.status,
            sortable: true
        },
        {
            name: 'Action',
            selector: row => <ActionButtons id={row._id} deleteAction={deleteCategory} path="/admin/category/"/>,
        }
    ];

    const getAllCategories = async () => {
        try{
            let response = await httpGetRequest('/category')
            if(response.status ){
                setData(response.result)
            }
        } catch(err){
            console.log("Exception: ", err)
        }
    }

    const [data, setData] = useState()
    useEffect(() => {
        getAllCategories()
    }, [])
       
    return (
        <>
            <div className="container-fluid px-4">
                <Breadcrumb
                    context="Category"
                    createUrl="/admin/category/create"
                    type="list"
                />
                <div className="card mb-4">
                    <div className="card-body">
                        <DataTable
                            columns={columns}
                            data={data}
                            pagination
                        />
                    </div>
                </div>
            </div>
        </>
    )
}

export default CategoryListComponent