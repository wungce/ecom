import CategoryListComponent from "./category-list.page";
import CategoryLayout from "./category-layout.page";
import CategoryCreateComponent from "./category-create.page";
import CategoryEditComponent from "./category-edit.component";

const Category = {
    CategoryListComponent,
    CategoryLayout,
    CategoryCreateComponent,
    CategoryEditComponent
    
}
export default Category