import { useFormik } from "formik"
import { Button, Col, Form } from "react-bootstrap"
import Breadcrumb from "../partials/breadcrumb.partials"
import * as Yup from 'yup'
import { httpGetRequest, httpPutRequest } from '../../../services/axios.service'
import { toast } from "react-toastify"
import { useNavigate, useParams } from "react-router-dom"
import { useCallback, useEffect, useState } from "react"
import Select from 'react-select'

const CategoryEditComponent = () => {
    let default_value = {
        title : '',
        status : '',
        image : '',
        brand : [],
        child_of : '',
        show_in_home : false
    }
    let validationSchema = Yup.object().shape({
        title: Yup.string().required('Title is required.'),
        status : Yup.string().required("Status is required"),
        image : Yup.object().nullable(),
        brand : Yup.array().nullable(),
        child_of : Yup.string().nullable(),
        show_in_home : Yup.boolean()
      })

    let param = useParams()

   
    const formik = useFormik({
        initialValues: default_value,
        validationSchema: validationSchema,
        onSubmit: async (values) => {
            try {
                const form_data = new FormData()

                console.log("values:", values)

                if(values.image && typeof(values.image) === "object"){
                    form_data.append('image', values.image, values.image.name)
                    delete values.image
                }else{
                    delete values.image
                }
                
                if(values.brand){
                    let sel_brands = values.brand.map((item) => item.value)
                    values['brands'] = sel_brands
                }
    
                Object.keys(values).map((key) => {
                    form_data.append(key, values[key])
                    return null ;
                })

                let response = await httpPutRequest('/category/'+param.id, form_data, true, true)
                console.log("response:", response)
                if (response.status) {
                    toast.success(response.msg)
                    navigate('/admin/category')
                } else {
                    toast.error(response.msg)
                }
            } catch (err) {
                console.error("Error: ", err)
            }
        }
    })

    let navigate = useNavigate()
    const getCategoryDetail = useCallback(async () => {
        let id = param.id
        try {
            let response = await httpGetRequest('/category/' +id)
            console.log(id)
            if (response.status) {
                let sel_cat = response.result.child_of ? response.result.child_of._id : null
                let sel_brands = response.result.brands.map((item) => {
                    return {
                        label : item.name,
                        value : item._id
                    }
                })
                formik.setValues({
                    ...formik.values,
                    ...response.result,
                    child_of : sel_cat,
                    brands : sel_brands
                })
            }
        } catch (err) {
            console.error(err)
        }
    }, [formik, param.id] )


     let [allCats, setAllCats] = useState()
      let [allBrands, setAllBrands] = useState();

      let getAllCategories = useCallback(
      async  () => {
            try{
                let response = await httpGetRequest('/category', true)
                if(response.status){
                    setAllCats(response.result)
                }
            } catch(err){
                console.error("Error :", err)
            }
        }
      ,[])

      let getAllBrands = useCallback(async() => {
            try{
                let response = await httpGetRequest('/brand', true)
                if(response){
                    let brands = response.result.map((item) => {
                        return {
                            label : item.name,
                            value : item._id
                        }
                    })
                    setAllBrands(brands)
                }
            } catch(error) {
                console.error(error)
            }
      },[])

      useEffect(() => {
        getAllCategories()
        getAllBrands()
        getCategoryDetail()
      },[getAllCategories, getAllBrands])



    return (
        <div className="container-fluid px-4">
            <Breadcrumb
                context="Category"
                createUrl=""
                type="Update"
            />
            <div className="card mb-4">
                <div className="card-body">
                <form onSubmit={formik.handleSubmit}>
                        <Form.Group className="row mb-3" controlId="title">
                            <Form.Label className="col-sm-3">Title:</Form.Label>
                           <Col sm={9}>
                           <Form.Control
                                type="text"
                                placeholder="Enter Category Title"
                                name="title"
                                size="sm"
                                required
                                value={formik.values.title}
                                onChange={formik.handleChange}
                            />
                            {
                                formik.errors.title && <em className="text-danger">{formik.errors.title}</em>
                            } 
                           </Col>
                        </Form.Group>
                       
                        <Form.Group className="row mb-3" controlId="child_of">
                            <Form.Label name="child_of" className="col-sm-3">Parent category</Form.Label>
                           <Col sm={9}>
                           <Form.Select size="sm" name="child_of"  value={formik.values.child_of || ''} onChange={formik.handleChange}>
                            <option value="">--select Any One</option>
                            {
                                allCats && allCats.map((item, i) => (
                                    <option value={item._id} key={i}>
                                        {item.title} 
                                    </option>
                                ))
                            }
                           </Form.Select>
                            {
                                formik.errors.child_of && <em className="text-danger">{formik.errors.child_of}</em>
                            } 
                           </Col>
                        </Form.Group>

                        <Form.Group className="row mb-3" controlId="status">
                            <Form.Label name="status" className="col-sm-3">Status :</Form.Label>
                           <Col sm={9}>
                           <Form.Select size="sm"  value={formik.values.status} onChange={formik.handleChange}>
                            <option value="">--select Any One</option>
                            <option value="active">Active</option>
                            <option value="inactive">In-Active</option>
                           </Form.Select>
                            {
                                formik.errors.status && <em className="text-danger">{formik.errors.status}</em>
                            } 
                           </Col>
                        </Form.Group>
                        
                        <Form.Group className="row mb-3" controlId="brand">
                            <Form.Label name="brand" className="col-sm-3">Brand :</Form.Label>
                           <Col sm={9}>
                           <Select options={allBrands} 
                           isMulti
                            onChange={(e) => {
                                formik.setValues({
                                    ...formik.values,
                                    brand : e
                                })
                            }}
                            value={formik.values.brand}
                            name="brand"
                            required
                            />
                            {
                                formik.errors.brand && <em className="text-danger">{formik.errors.brand}</em>
                            } 
                           </Col>
                        </Form.Group>
                        
                        <Form.Group className="row mb-3" controlId="show_in_home">
                            <Form.Label name="show_in_home" className="col-sm-3">Display In Home Panel :</Form.Label>
                           <Col sm={9}>
                           <Form.Check 
                                type= "checkbox"
                                label="yes"
                                name="show_in_home"
                                checked={formik.values.show_in_home}
                                onChange={formik.handleChange}
                            />
                            {
                                formik.errors.show_in_home && <em className="text-danger">{formik.errors.show_in_home}</em>
                            } 
                           </Col>
                        </Form.Group>

                        <Form.Group className="row mb-3" controlId="image">
                            <Form.Label className="col-sm-3">Image:</Form.Label>
                           <Col sm={3}>
                           <Form.Control
                                type="file"
                                name="image"
                                size="sm"
                                onChange={(e) => {
                                    formik.setValues({
                                        ...formik.values,
                                        image : e.target.files[0]
                                    })
                                }}
                            />
                            {
                                formik.errors.image && <em className="text-danger">{formik.errors.image}</em>
                            }  
                           </Col>
                           <Col sm={3}>
                            {
                                formik.values.image && typeof(formik.values.image) === "string" ?
                                <img className="img img-fluid" src={process.env.REACT_APP_IMAGE_URL+'/category/'+formik.values.image} alt="" />
                                :
                                <img className="img img-fluid" src={formik.values.image && URL.createObjectURL(formik.values.image)} alt="" />
                            }
                           </Col>
                        </Form.Group> 

                        <Form.Group className="row mb-3" controlId="submit">
                           <Col sm={{offset:3, span:9}}>
                            <Button variant="danger" size="sm" type="reset" className="me-2">
                                <i className="fa fa-trash "></i>Rest
                            </Button>
                            <Button variant="success" size="sm" type="submit">
                                <i className="fa fa-paper-plane "></i>Submit
                            </Button>
                          </Col>
                        </Form.Group>

                        
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CategoryEditComponent