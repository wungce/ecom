import { useFormik } from "formik"
import { Button, Col, Form } from "react-bootstrap"
import Breadcrumb from "../partials/breadcrumb.partials"
import * as Yup from 'yup'
import { httpGetRequest, httpPutRequest } from '../../../services/axios.service'
import { toast } from "react-toastify"
import { useNavigate, useParams } from "react-router-dom"
import { useEffect } from "react"
const BannerEditComponent = () => {
    let default_value = {
        name: '',
        link: '',
        status: '',
        image: ''
    }

    let param = useParams()

    let validationSchema = Yup.object().shape({
        name: Yup.string().required('Title is required.'),
        link: Yup.string(),
        status: Yup.string().required("Status is required"),
        image: Yup.object().nullable()
    })
    
    
    const formik = useFormik({
        initialValues: default_value,
        validationSchema: validationSchema,
        onSubmit: async (values) => {
            try {
                console.log(values)
                const form_data = new FormData()

                // if(values.image){
                //     form_data.append('image', values.image, values.image.name)
                //         delete values.image
                // }
                if(values.image && typeof(values.image) === 'object'){
                    form_data.append('image', values.image, values.image.name)
                        delete values.image
                }else{
                    delete values.image
                }

                Object.keys(values).map((key) => {
                    form_data.append(key, values[key])
                    return null;
                })

                let response = await httpPutRequest("/banner/"+param.id, form_data, true, true)
                console.log("response:", response)
                if (response.status) {
                    toast.success(response.msg)
                    navigate('/admin/banner')
                } else {
                    toast.error(response.msg)
                }
            } catch (err) {
                console.error("Error: ", err)
            }
        }
    })

    let navigate = useNavigate()


    const getBannerDetail = async () => {
        let id = param.id
        try {
            let response = await httpGetRequest('/banner/'+id)
            console.log(response)
            if (response.status) {
                formik.setValues({
                    ...formik.values,
                    ...response.result
                })
            }
        } catch (err) {
            console.error(err)
        }
    }

    useEffect(() => {
        getBannerDetail()
    },[])

    return (
        <div className="container-fluid px-4">
            <Breadcrumb
                context="Banner"
                createUrl=""
                type="Update"
            />
            <div className="card mb-4">
                <div className="card-body">
                    <form onSubmit={formik.handleSubmit}>
                        <Form.Group className="row mb-3" controlId="name">
                            <Form.Label className="col-sm-3">Title:</Form.Label>
                            <Col sm={9}>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter Banner Title"
                                    name="name"
                                    size="sm"
                                    required
                                    value={formik.values.name}
                                    onChange={formik.handleChange}
                                />
                                {
                                    formik.errors.name && <em className="text-danger">{formik.errors.name}</em>
                                }
                            </Col>
                        </Form.Group>
                        <Form.Group className="row mb-3" controlId="link">
                            <Form.Label className="col-sm-3">Link:</Form.Label>
                            <Col sm={9}>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter Banner Link"
                                    name="link"
                                    size="sm"
                                    value={formik.values.link}
                                    onChange={formik.handleChange}
                                />
                                {
                                    formik.errors.link && <em className="text-danger">{formik.errors.link}</em>
                                }
                            </Col>
                        </Form.Group>
                        <Form.Group className="row mb-3" controlId="status">
                            <Form.Label className="col-sm-3">Status :</Form.Label>
                            <Col sm={9}>
                                <Form.Select size="sm" required value={formik.values.status} onChange={formik.handleChange}>
                                    <option>--select Any One</option>
                                    <option value="active">Active</option>
                                    <option value="inactive">In-Active</option>
                                </Form.Select>
                                {
                                    formik.errors.status && <em className="text-danger">{formik.errors.status}</em>
                                }
                            </Col>
                        </Form.Group>


                        <Form.Group className="row mb-3" controlId="image">
                            <Form.Label className="col-sm-3">Image:</Form.Label>
                            <Col sm={3}>
                                <Form.Control
                                    type="file"
                                    name="image"
                                    size="sm"
                                    onChange={(e) => {
                                        formik.setValues({
                                            ...formik.values,
                                            image: e.target.files[0]
                                        })
                                    }}
                                />
                                {
                                    formik.errors.image && <em className="text-danger">{formik.errors.image}</em>
                                }
                            </Col>
                            <Col sm={3}>
                                {
                                    formik.values.image && typeof (formik.values.image) === 'string'
                                        ?
                                        <img className="img img-fluid" src={process.env.REACT_APP_IMAGE_URL+"/banner/"+formik.values.image} alt="" />
                                        :
                                        <img className="img img-fluid" src={formik.values.image && URL.createObjectURL(formik.values.image)} alt="" />

                                }
                            </Col>
                        </Form.Group>

                        <Form.Group className="row mb-3" controlId="submit">
                            <Col sm={{ offset: 3, span: 9 }}>
                                <Button variant="danger" size="sm" type="reset" className="me-2">
                                    <i className="fa fa-trash "></i>Rest
                                </Button>
                                <Button variant="success" size="sm" type="submit">
                                    <i className="fa fa-paper-plane "></i>Submit
                                </Button>
                            </Col>
                        </Form.Group>


                    </form>
                </div>
            </div>
        </div>
    )
}

export default BannerEditComponent