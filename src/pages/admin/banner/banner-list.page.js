import Breadcrumb from "../partials/breadcrumb.partials"
import DataTable from 'react-data-table-component';
import { useState, useEffect } from "react";
import { httpDeleteRequest, httpGetRequest } from "../../../services/axios.service";
import { Imageview } from "../../../component/common/image-view/image-view.component";
import { ActionButtons } from "../../../component/common/action-btns/action-buttons.component";
import { toast } from "react-toastify";

const BannerListComponent = () => {
    const deleteBanner = async (id) => {
        try{
            let response = await httpDeleteRequest('/banner/'+id, true)
            console.log(response)
            if(response.status){
                toast.success(response.msg)
                getAllBanners()
            } else{
                toast.error(response.msg)
            }
        } catch(err){
            console.error("DeleteErr:", err)
        }
    }
    const columns = [
        {
            name: 'Title',
            selector: row => row.name,
            sortable: true
        },
        {
            name: 'Link',
            selector: row => row.link ? <a className="btn-link" href={row.link}>{row.link}</a> : ""
        },
        {
            name: 'Image',
            selector: row => <Imageview path={row.image} dir="banner"/>,
        },
        {
            name: 'Status',
            selector: row => row.status,
            sortable: true
        },
        {
            name: 'Action',
            selector: row => <ActionButtons id={row._id} deleteAction={deleteBanner} path="/admin/banner/"/>,
        }
    ];

    const getAllBanners = async () => {
        try{
            let response = await httpGetRequest('/banner')
            if(response.status ){
                setData(response.result)
            }
        } catch(err){
            console.log("Exception: ", err)
        }
    }

    const [data, setData] = useState()
    useEffect(() => {
        getAllBanners()
    }, [])
       
    return (
        <>
            <div className="container-fluid px-4">
                <Breadcrumb
                    context="Banner"
                    createUrl="/admin/banner/create"
                    type="list"
                />
                <div className="card mb-4">
                    <div className="card-body">
                        <DataTable
                            columns={columns}
                            data={data}
                            pagination
                        />
                    </div>
                </div>
            </div>
        </>
    )
}

export default BannerListComponent