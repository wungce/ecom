import { NavLink } from "react-router-dom"
const AdminSidebar = () => {

    return (
        <>
            <div id="layoutSidenav_nav">
                <nav className="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
                    <div className="sb-sidenav-menu">
                        <div className="nav">
                            <div className="sb-sidenav-menu-heading">Core</div>
                            <NavLink className="nav-link" to="/admin">
                                <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </NavLink>
                            <NavLink className="nav-link" to="/">
                                <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                Go To Home
                            </NavLink>
                            <NavLink className="nav-link" to={'/admin/banner'}>
                                <div className="sb-nav-link-icon"><i className="fas fa-images"></i></div>
                                Banner
                            </NavLink>
                            <NavLink className="nav-link" to="/admin/brand">
                                <div className="sb-nav-link-icon"><i className="fas fa-b"></i></div>
                                Brands
                            </NavLink>
                            <NavLink className="nav-link" to="/admin/category">
                                <div className="sb-nav-link-icon"><i className="fas fa-sitemap"></i></div>
                                Categories
                            </NavLink>
                            <NavLink className="nav-link" to="/admin/user">
                                <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                Users
                            </NavLink>
                            <NavLink className="nav-link" to="/admin/product">
                                <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                Products
                            </NavLink>
                            <NavLink className="nav-link" to="/admin/order">
                                <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                Order
                            </NavLink>
                            <NavLink className="nav-link" to="index.html">
                                <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                Reviews
                            </NavLink>
                        </div>
                    </div>
                    <div className="sb-sidenav-footer">
                        <div className="small">Logged in as:</div>
                        Start Bootstrap
                    </div>
                </nav>
            </div>
        </>
    )
}

export default AdminSidebar