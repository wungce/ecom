import AdminTopMenu from './to-men.partials'
import AdminSidebar from './sidebar.partials'
import AdminFooter from './footer.partials'

const AdminPartials = {
    AdminTopMenu,
    AdminSidebar,
    AdminFooter
}

export default AdminPartials