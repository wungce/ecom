import UserList from "./user-list.page";
import UserLayout from "./user-layout.page";

const User = {
    UserList,
    UserLayout
}

export default User