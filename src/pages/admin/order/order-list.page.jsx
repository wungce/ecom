import React from 'react'
import Breadcrumb from '../partials/breadcrumb.partials'
import DataTable from 'react-data-table-component';
import { useState, useEffect } from "react";
import { httpDeleteRequest, httpGetRequest } from "../../../services/axios.service";
import { ActionButtons } from "../../../component/common/action-btns/action-buttons.component";
import { toast } from "react-toastify";


const OrderList = () => {
  const deleteCart = async (id) => {
    try{
        let response = await httpDeleteRequest('/cart/'+id, true)
        console.log(response)
        if(response.status){
            toast.success(response.msg)
            getAllOrders()
        } else{
            toast.error(response.msg)
        }
    } catch(err){
        console.error("DeleteErr:", err)
    }
}
  const columns = [
    {
        name: 'Customer Id',
        selector: row => row.customer_id,
        sortable: true
    },
    {
        name: 'CartItems',
        selector: row => row.cart_items.map((item) => item.product_id),
        sortable: true
    },
    {
        name: 'Total amount',
        selector: row => row.total_amount,
        sortable: true
    },
    // {
    //     name: 'Action',
    //     selector: row => <ActionButtons id={row._id} deleteAction={deleteCart} />,
    // }
];

const getAllOrders = async () => {
  try{
      let response = await httpGetRequest('/cart/list', true)
      console.log(response.result)
      if(response.status ){
          setData(response.result)
      }
  } catch(err){
      console.log("Exception: ", err)
  }
}

const [data, setData] = useState()
useEffect(() => {
  getAllOrders()
}, [])
 
  return (
    <>
    <div className="container-fluid px-4">
                <Breadcrumb
                    context="Order"
                    type="list"
                />
                <div className="card mb-4">
                    <div className="card-body">
                        <DataTable
                            columns={columns}
                            data={data}
                            pagination
                        />
                    </div>
                </div>
            </div>
            </>
  )
}

export default OrderList