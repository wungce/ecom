import OrderList from "./order-list.page";
import OrderLayout from "./order-layout.page";

const Order = {
    OrderList,
    OrderLayout
}

export default Order