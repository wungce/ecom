import Breadcrumb from "../partials/breadcrumb.partials"
import DataTable from 'react-data-table-component';
import { useState, useEffect } from "react";
import { httpDeleteRequest, httpGetRequest } from "../../../services/axios.service";
import { Imageview } from "../../../component/common/image-view/image-view.component";
import { ActionButtons } from "../../../component/common/action-btns/action-buttons.component";
import { toast } from "react-toastify";

const BrandListComponent = () => {
    const deleteBrand = async (id) => {
        try{
            let response = await httpDeleteRequest('/brand/'+id, true)
            if(response.status){
                toast.success(response.msg)
                getAllBrands()
            } else{
                toast.error(response.msg)
            }
        } catch(err){
            console.error("DeleteErr:", err)
        }
    }
    const columns = [
        {
            name: 'Title',
            selector: row => row.name,
            sortable: true
        },
        {
            name: 'Image',
            selector: row => <Imageview path={row.image} dir="brand"/>,
        },
        {
            name: 'Status',
            selector: row => row.status,
            sortable: true
        },
        {
            name: 'Action',
            selector: row => <ActionButtons id={row._id} deleteAction={deleteBrand} path="/admin/brand/"/>,
        }
    ];

    const getAllBrands = async () => {
        try{
            let response = await httpGetRequest('/brand')
            if(response.status ){
                setData(response.result)
            }
        } catch(err){
            console.log("Exception: ", err)
        }
    }

    const [data, setData] = useState()
    useEffect(() => {
        getAllBrands()
    }, [])
       
    return (
        <>
            <div className="container-fluid px-4">
                <Breadcrumb
                    context="Brand"
                    createUrl="/admin/brand/create"
                    type="list"
                />
                <div className="card mb-4">
                    <div className="card-body">
                        <DataTable
                            columns={columns}
                            data={data}
                            pagination
                        />
                    </div>
                </div>
            </div>
        </>
    )
}

export default BrandListComponent