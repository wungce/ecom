import BrandListComponent from "./brand-list.page";
import BrandLayout from "./brand-layout.page";
import BrandCreateComponent from "./brand-create.page";
import BrandEditComponent from "./brand-edit.component";

const Banner = {
    BrandListComponent,
    BrandLayout,
    BrandCreateComponent,
    BrandEditComponent
    
}
export default Banner