import { createSlice } from "@reduxjs/toolkit";



export const UserSlicer = createSlice({
    name : "user",
    initialState : {
        all_admins: []
    },
    reducers : {
        setAllAdmin:  (state, action) => {
                state.all_admins = action.payload
          
        },
        getAllAdmins: (state, action) => {
            return state.all_admins
        }
    }
})

export const {setAllAdmin, getAllAdmins} = UserSlicer.actions
 
export default UserSlicer.reducer