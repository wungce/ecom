import { createSlice } from "@reduxjs/toolkit";

export const CartSlicer = createSlice({
    name: "cart",
    initialState: {
        cart: []
    },
    reducers: {
        addToCart: (state, action) => {
            let prev_cart = JSON.parse(localStorage.getItem("cart_8")) || [];
            if(!prev_cart || prev_cart.length <= 0) {
                prev_cart.push(action.payload)
            }else{
                let index = null;
                prev_cart.map((item, ind) => {
                     if(item.product_id === action.payload.product_id){
                        index = ind
                     }
                })
                if(index == null){
                    prev_cart.push(action.payload);
                }else{
                    prev_cart[index].qty = action.payload.qty 
                }
            }

            state.cart = prev_cart
            localStorage.setItem("cart_8", JSON.stringify(prev_cart))
        },
        updateCart: (state, action) => {
           
            state.cart = action.payload
            localStorage.setItem("cart_8", JSON.stringify(action.payload))
        },
        updateItemInCart: (state, action) => {
            // console.log("action", action.payload )
            let all_items = action.payload.all_items
            let index = action.payload.index;
            let qty = action.payload.total_qty;
            let item = all_items[index];
            item = {
                ...item,
                qty: qty
            }
            all_items = {
                ...all_items,
                [index]: item
            }
            state.cart = Object.values(all_items)
            localStorage.setItem("cart_8", JSON.stringify(state.cart))

        },
        setCart : (state, action) => {
            state.cart = action.payload
        }
    }
})

export const { addToCart, updateCart, updateItemInCart, setCart } = CartSlicer.actions

export default CartSlicer.reducer