import { Provider, useDispatch, useSelector } from "react-redux"
import { BrowserRouter, Navigate, Route, Routes, useNavigate } from "react-router-dom"
import AdminPages from "../pages/admin"
import FrontPage from "../pages/front"
import AdminLayout from "../pages/layouts/admin-layout.page"
import FrontLayout from "../pages/layouts/front-layout.page"
import SellerLayout from "../pages/layouts/seller-layout.page"
import store from "../store"
import { toast, ToastContainer } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import { useCallback, useEffect } from "react"
import { httpPostRequest } from "../services/axios.service"
import { setCart } from "../reducers/cart.reducer"

const Checkout = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const cart = useSelector((store) => {
        return store.cart.cart;
    })
    const CheckoutCart = useCallback(async() => { 
        try{
           if(cart && cart.length > 0){
            let response = await httpPostRequest('/cart/create', cart, true)
            if(response.status){
                toast.success(response.msg);
                localStorage.removeItem('cart_8')
                dispatch(setCart([]))
                navigate('/')
            }
           }
        } catch(error){
            console.log("This is error:",error)
        }
    }, [dispatch, navigate, cart])

    useEffect(() => {
        CheckoutCart()

    }, [CheckoutCart])

    return (
        <>Checkout </>
    )
}

const PrivateRoute = ({component}) => {
    //  let component = props.component
     let is_logged_in = localStorage.getItem('access_token') ?? null;
     return (
        is_logged_in ? component : <Navigate to="/login"/>
     )
}

const Routing = () => {

    return (
        <BrowserRouter>
        <ToastContainer />
           <Provider store={store}>
           <Routes>
                <Route path="/" element={<FrontLayout />}>

                    <Route index element={<FrontPage.HomePage />} />
                    <Route path="login" element={<FrontPage.LoginPage />} />
                    <Route path="register" element={<FrontPage.RegisterPage />} />
                    <Route path="category/:slug" element={<FrontPage.CategoryPage />} />
                    <Route path="product/:slug" element={<FrontPage.ProductPage/>} />
                    <Route path="*" element={<FrontPage.CartPage/>}/>
                    <Route path="*" element={<FrontPage.ErrorPage />} />
                    <Route path="checkout" element={<PrivateRoute component={<Checkout />} />} ></Route>

                </Route>

                <Route path="/admin" element={<PrivateRoute component={<AdminLayout/>} />} >
                    <Route index element={<AdminPages.DashboardPage/>}/>
                    
                    <Route path="banner" element={<AdminPages.Banner.BannerLayout />}>
                        <Route index element={<AdminPages.Banner.BannerListComponent/>}></Route>
                        <Route path="create" element={<AdminPages.Banner.BannerCreateComponent/>}></Route>
                        <Route path=":id" element={<AdminPages.Banner.BannerEditComponent/>}></Route>
                    </Route>

                    <Route path="brand" element={<AdminPages.Brand.BrandLayout />}>
                        <Route index element={<AdminPages.Brand.BrandListComponent/>}></Route>
                        <Route path="create" element={<AdminPages.Brand.BrandCreateComponent/>}></Route>
                        <Route path=":id" element={<AdminPages.Brand.BrandEditComponent/>}></Route>
                    </Route>

                    <Route path="category" element={<AdminPages.Category.CategoryLayout />}>
                        <Route index element={<AdminPages.Category.CategoryListComponent/>}></Route>
                        <Route path="create" element={<AdminPages.Category.CategoryCreateComponent/>}></Route>
                        <Route path=":id" element={<AdminPages.Category.CategoryEditComponent/>}></Route>
                    </Route>

                    <Route path="product" element={<AdminPages.Product.ProductLayout />}>
                        <Route index element={<AdminPages.Product.ProductListComponent/>}></Route>
                        <Route path="create" element={<AdminPages.Product.ProductCreateComponent/>}></Route>
                        <Route path=":id" element={<AdminPages.Product.ProductEditComponent/>}></Route>
                    </Route>

                    <Route path="order" element={<AdminPages.Order.OrderLayout />} >
                        <Route index element={<AdminPages.Order.OrderList />} /> 
                    </Route>

                    <Route path="user" element={<AdminPages.User.UserLayout />} >
                        <Route index element={<AdminPages.User.UserList />} /> 
                    </Route>


                </Route>

                <Route path="/seller" element={<PrivateRoute component={<SellerLayout/>}/>} >

                </Route>
            </Routes>
           </Provider>
        </BrowserRouter>
    )

}

export default Routing