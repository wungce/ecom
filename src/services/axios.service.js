import axios from 'axios'

const axiosInstance = axios.create({
    baseURL : "http://localhost:3005/api/v1",
    timeout : 3000,
    timeoutErrorMessage : "Server time out.",
    headers : {
        "content-type" : "application/json"
    }
})
axiosInstance.interceptors.response.use((response) => {
    if(response.status === 200 || response.status === 201){
        return response.data
    }else if(response.status === 401){
        localStorage.clear()
        return response
    }else{
        console.log("Error:", response)
        return response
    }
})
let headers = {}

const setHeaders = (is_strict, form_data = false) => {
    headers = {
        headers : {
            "content-type" : "application/json"
        }
    }
    // login process login must be required
    if(is_strict){     
    let token = localStorage.getItem('access_token')
        headers = {
            headers:{
                ...headers.headers,
                "authorization" : "Bearer "+token
            }
        }
    }
    // it's mean form data or file uploading process that get it
    if(form_data){
        headers = {
            headers:{
                ...headers.headers,
                "content-type" : "multipart/form-data"
            }
        }
    }
}
export const httpPostRequest = (url, data, is_strict = false, form_data = false) => {
   setHeaders(is_strict, form_data)
    return axiosInstance.post(url, data, headers)
}

export const httpGetRequest = (url, is_strict = false) => {
    setHeaders(is_strict)
    return axiosInstance.get(url, headers)
}

export const httpPutRequest = (url, data,  is_strict = false, form_data = false) => {
    setHeaders(is_strict, form_data)
    return axiosInstance.put(url,data, headers)
}
export const httpPatchRequest = (url, data,  is_strict = false, form_data = false) => {
    setHeaders(is_strict, form_data)
    return axiosInstance.patch(url,data, headers)
}

export const httpDeleteRequest = (url, is_strict = false) => {
    setHeaders(is_strict)
    return axiosInstance.delete(url, headers)
}