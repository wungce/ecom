import axios from 'axios'
const axiosInstance = axios.create({
    baseURL : "http://localhost:3005/api/v1",
    timeout : 30000,
    timeoutErrorMessage : "Server time out",
    headers : {
        'content-type' : 'application/json'
    }
})

axiosInstance.response.use((response) =>{
    if(response.status === 200 || response.status === 201){
        return response.data
    }else{
    return response        
    }
})

export const httpPostRequest = (url, data) =>{
    return axiosInstance.post(url, data)
}

// const response = httpPostRequest('/login', value)