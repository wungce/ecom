import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./reducers/cart.reducer";
import userReducer from "./reducers/user.reducer";


//It is central store of this file.It is receive from reducer
const store = configureStore({
    reducer : {
        user : userReducer,
        cart : cartReducer
    }
})

export default store 